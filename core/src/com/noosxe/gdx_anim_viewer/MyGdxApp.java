package com.noosxe.gdx_anim_viewer;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.assets.loaders.resolvers.ExternalFileHandleResolver;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.PerspectiveCamera;
import com.badlogic.gdx.graphics.g3d.*;
import com.badlogic.gdx.graphics.g3d.attributes.BlendingAttribute;
import com.badlogic.gdx.graphics.g3d.attributes.ColorAttribute;
import com.badlogic.gdx.graphics.g3d.environment.DirectionalLight;
import com.badlogic.gdx.graphics.g3d.loader.G3dModelLoader;
import com.badlogic.gdx.graphics.g3d.model.Animation;
import com.badlogic.gdx.graphics.g3d.utils.AnimationController;
import com.badlogic.gdx.graphics.g3d.utils.CameraInputController;
import com.badlogic.gdx.math.collision.BoundingBox;
import com.badlogic.gdx.scenes.scene2d.*;
import com.badlogic.gdx.scenes.scene2d.ui.*;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.GdxRuntimeException;
import com.badlogic.gdx.utils.UBJsonReader;
import com.badlogic.gdx.utils.viewport.ScreenViewport;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.io.File;
import java.util.ArrayList;

public class MyGdxApp extends ApplicationAdapter {

    public final static String LOG_TAG = "ANIM VIEWER";

    private PerspectiveCamera camera;

    private AssetManager assetManager;
    private G3dModelLoader g3dbModelLoader;
    private ModelBatch modelBatch;

    private boolean loading;

    private Model model;
    private ModelInstance instance;
    private AnimationController animationController;

    private String modelPath;

    private CameraInputController camController;

    private Environment environment;

    private Stage stage;
    private Table table;
    private SelectBox<String> selectBox;
    private Label durationLabel;
    private Label currentTimeLabel;

    private String currentAnimName;

    private int durationTime;
    private int currentTime;

	@Override
	public void create () {
        g3dbModelLoader = new G3dModelLoader(new UBJsonReader());
        assetManager = new AssetManager(new ExternalFileHandleResolver());
        modelBatch = new ModelBatch();
        stage = new Stage(new ScreenViewport());
        table = new Table();

        table.setFillParent(true);
//        table.setDebug(true);

        environment = new Environment();
        environment.set(new ColorAttribute(ColorAttribute.AmbientLight, 0.4f, 0.4f, 0.4f, 1f));
        environment.add(new DirectionalLight().set(0.8f, 0.8f, 0.8f, -1f, -0.8f, -0.2f));

        camera = new PerspectiveCamera(67, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        camera.position.set(0f, 0f, 40f);
        camera.lookAt(0,0,0);
        camera.near = 1f;
        camera.far = 300f;
        camera.update();

        InputMultiplexer multi = new InputMultiplexer();

        camController = new CameraInputController(camera);

        multi.addProcessor(stage);
        multi.addProcessor(camController);

        Gdx.input.setInputProcessor(multi);

        Skin skin = new Skin(Gdx.files.internal("uiskin.json"));

        TextButton loadButton = new TextButton("Load", skin);

        loadButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                final JFileChooser fc = new JFileChooser();

                FileNameExtensionFilter g3dbFilter = new FileNameExtensionFilter("g3db files (*.g3db)", "g3db");
                fc.setFileFilter(g3dbFilter);
                int status = fc.showOpenDialog(null);

                if (status == JFileChooser.APPROVE_OPTION) {
                    final File selectedFile = fc.getSelectedFile();
                    Gdx.app.postRunnable(new Runnable() {
                        @Override
                        public void run() {
                            load(selectedFile);
                        }
                    });
                }
            }
        });

        selectBox = new SelectBox<>(skin);

        selectBox.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                currentAnimName = selectBox.getSelected();

                Animation currentAnim = null;

                for (Animation anim: instance.animations) {
                    if (anim.id.equals(currentAnimName)) {
                        currentAnim = anim;
                    }
                }

                if (currentAnim != null) {
                    durationTime = (int)(currentAnim.duration * 1000);
                }

                showDuration();

                currentTime = 0;

                animationController.setAnimation(currentAnimName, 0, 0, 1, 1f, null);
            }
        });

        stage.addActor(table);

        table.left().bottom();
        table.add(loadButton).pad(0, 2, 0, 2);
        table.add(selectBox).pad(0, 2, 0, 2);

        TextButton prevButton = new TextButton("<", skin);
        TextButton nextButton = new TextButton(">", skin);

        prevButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                currentTime -= 33;
                currentTime = Math.max(currentTime, 0);
                updateAnimation();
            }
        });

        nextButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                currentTime += 33;
                currentTime = Math.min(currentTime, durationTime);
                updateAnimation();
            }
        });

        table.add(prevButton).pad(0, 2, 0, 2);
        table.add(nextButton).pad(0, 2, 0, 2);

        durationLabel = new Label(String.format("duration: %dms", durationTime), skin);
        currentTimeLabel = new Label(String.format("current: %dms", currentTime), skin);

        table.add(durationLabel).pad(0, 2, 0, 2);
        table.add(currentTimeLabel).pad(0, 2, 0, 2);

        TextButton playButton = new TextButton("play", skin);

        playButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                if (currentAnimName != null) {
                    animationController.setAnimation(currentAnimName);
                }
            }
        });

        table.add(playButton).pad(0, 2, 0, 2);
	}

	@Override
	public void render () {
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT);

        if (animationController != null) {
            animationController.update(Gdx.graphics.getDeltaTime());
        }

        if (instance != null) {
            modelBatch.begin(camera);
            modelBatch.render(instance, environment);
            modelBatch.end();
        }

        stage.act(Gdx.graphics.getDeltaTime());
        stage.draw();
	}

    public void resize (int width, int height) {
        stage.getViewport().update(width, height, true);

        camera.viewportWidth = 30f;
        camera.viewportHeight = 30f * height/width;
        camera.update();
    }

    public void dispose() {
        stage.dispose();
    }

    private void updateAnimation() {
        if (animationController == null) {
            return;
        }

        currentTimeLabel.setText(String.format("current: %dms", currentTime));

        animationController.setAnimation(currentAnimName, currentTime / 1000f, 0, 1, 1f, new AnimationController.AnimationListener() {
            @Override
            public void onEnd(AnimationController.AnimationDesc animation) {

            }

            @Override
            public void onLoop(AnimationController.AnimationDesc animation) {

            }
        });
    }

    private void showDuration() {
        durationLabel.setText(String.format("duration: %dms", durationTime));
    }

    private void load(File f) {
        try {
            model = g3dbModelLoader.loadModel(Gdx.files.absolute(f.getAbsolutePath()));
        } catch (GdxRuntimeException e) {
            JOptionPane.showMessageDialog(null, e.getMessage());
            return;
        }

        instance = new ModelInstance(model);
        animationController = new AnimationController(instance);
        animationController.allowSameAnimation = true;

        instance.transform.setToScaling(0.1f, 0.1f, 0.1f);

        for (Material mat: instance.materials) {
            mat.set(new BlendingAttribute(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA));
        }

        ArrayList<String> animNames = new ArrayList<>();

        for (Animation animation: instance.animations) {
            animNames.add(animation.id);
        }

        selectBox.setItems(animNames.toArray(new String[0]));

        BoundingBox bb = new BoundingBox();

        instance.calculateBoundingBox(bb).mul(instance.transform);

        Gdx.app.debug(LOG_TAG, bb.getWidth() + "x" + bb.getHeight());
    }
}
